#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    
    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 200)
        
    def test_read_3(self):
        s = "340     220\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  340)
        self.assertEqual(j, 220)
        
    def test_read_4(self):
        s = "101 374\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  101)
        self.assertEqual(j, 374)
        
    def test_read_5(self):
        s = "318 24\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  318)
        self.assertEqual(j, 24)
        
    def test_read_6(self):
        s = "1   999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 999999)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
        
    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)
        
    def test_eval_6(self):
        v = collatz_eval(200, 200)
        self.assertEqual(v, 27)
    
    def test_eval_7(self):
        v = collatz_eval(138, 11)
        self.assertEqual(v, 122)
        
    def test_eval_8(self):
        v = collatz_eval(999990, 999999)
        self.assertEqual(v, 259)
    
    def test_eval_9(self):
        v = collatz_eval(1249, 2477)
        self.assertEqual(v, 209)
    
    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
        
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 138, 11, 122)
        self.assertEqual(w.getvalue(), "138 11 122\n")
        
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 200, 200, 27)
        self.assertEqual(w.getvalue(), "200 200 27\n")     
        
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")    
        
    def test_print_5(self):
        w = StringIO()
        collatz_print(w, 999000, 11, 525)
        self.assertEqual(w.getvalue(), "999000 11 525\n")
        

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        
    def test_solve_2(self):
        r = StringIO("900 1000\n1 1\n200 200\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "900 1000 174\n1 1 1\n200 200 27\n")
        
    def test_solve_3(self):
        r = StringIO("1 10000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10000 262\n")
    
    def test_solve_4(self):
        r = StringIO("1 2\n1 3\n1 4\n1 6\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 2 2\n1 3 8\n1 4 8\n1 6 9\n")
        
    def test_solve_5(self):
        r = StringIO("124 32\n1 999999\n999999 2\n999998 999999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "124 32 119\n1 999999 525\n999999 2 525\n999998 999999 259\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
